@RestResource(urlMapping='/getLocations') 
global class RetriveLocations {
    
    @HttpGet
    global static void getLocations()
    {
        Utils.retriveLocationHelper returnObject= new Utils.retriveLocationHelper();
        RestContext.response.addHeader('Content-Type', 'application/json');
      	RestContext.response.addHeader('Access-Control-Allow-Origin', '*');
        RestContext.response.responseBody = Blob.valueOf(JSON.serialize(returnObject));
 
    }   
}