@isTest
public class test_endUserWidgetSaveAPI 
{
static testMethod void testEndUserWidgetMonthlyCalendarAndEvents() 
{        
   Location__c testLocation  = TestDataUtils.getlocation();
        Category__c testCategory = TestDataUtils.getcategory();
        Resource_Type__c resTyp = TestDataUtils.getResourceType();
        Contact servicePrvider  = TestDataUtils.getServiceProviderWithResTyp(resTyp);
        Location_Category_Mapping__c lcm = TestDataUtils.getlocationmapping(testLocation,testCategory,servicePrvider);
        AppointmentType__c appTyp =  TestDataUtils.getappointment(testCategory);
        Resource_Type_Appointment_Type_Mapping__c resTypMap =TestDataUtils.getresaptypemappingSpecific(resTyp,appTyp,servicePrvider);
        Resource_Type__c resTypNH = TestDataUtils.getResourceTypeNH();
         Resource_Type__c resTypass = TestDataUtils.getResourceTypeAssistant();
         Contact assistant=TestDataUtils.getAssistant(resTypass);
         Location_Category_Mapping__c Locat1=TestDataUtils.getlocationmapping(testLocation,testCategory,Assistant);
        TestDataUtils.getresaptypemapping(resTypNH,appTyp);
         TestDatautils.getresaptypemapping(resTypass,appTyp);
         Event e1=TestDataUtils.insertWorkingTimeSlot(servicePrvider, resTyp, lcm);
         TestDataUtils.insertWorkingTimeSlot(assistant, resTypass, Locat1);
        Resources__c r2 = TestDataUtils.getresources(testLocation,resTypNH);
        Resources__c r3 = TestDataUtils.getresources(testLocation,resTypNH);
        Resources__c r4 = TestDataUtils.getresources(testLocation,resTypNH);
        Resources__c r5 = TestDataUtils.getresources(testLocation,resTypNH);
        Booked_Resources__c br = TestDataUtils.getbookedresources();
        TestDataUtils.getactualbooked(r2,br);
         TestDataUtils.getactualbookedAssistant(assistant, br);
        TestDataUtils.getAppointmentWithBookedResource(servicePrvider,lcm,appTyp,br);
    Datetime Datetimenow=datetime.now();
        //do POST request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/endUserWidget/save';
        req.httpMethod = 'POST';
    req.addParameter('alsoAddMeToWaitlist','false');
        req.addParameter('appointmentDate',String.valueof(Datetimenow.day())+','+String.valueof(Datetimenow.month())+','+String.valueof(Datetimenow.year()));
        req.addParameter('isAnyPractitioner','false');
        req.addParameter('isClassApp','true');
        req.addParameter('newappBirthDate','');
        req.addParameter('newappPhone','654132165845');
        req.addParameter('newappemail','Caop@gmail.com');
        req.addParameter('newappfname','Coap');
        req.addParameter('newapplname','Test');
        req.addParameter('startTime','15:00');
        req.addParameter('endTime','16:00');
        req.addParameter('newappPhone','555555555555');
        req.addParameter('newappComments','sample desc');
        req.addParameter('selectLocationfornewapp',testLocation.id);
        req.addParameter('selectCategoryfornewapp',testCategory.id);
        req.addParameter('selectatfornewapp',appTyp.id);
        req.addParameter('selectPractitionerfornewapp',servicePrvider.id);
        RestContext.request = req;
        RestContext.response = res;

        endUserWidgetSaveAPI.saveAppointment();
    System.assertNotEquals(null, res.responseBody);
    }
    static testMethod void testEndUserWidgetsavenotclass() 
{       
    contact sp=TestDataUtils.getServiceProvider();
         Category__c Cat=TestDataUtils.getcategory();
         Location__c loc=TestDataUtils.getlocation();
         Location_Category_Mapping__c Locat=TestDataUtils.getlocationmapping(loc,Cat,sp);
         AppointmentType__c atype=TestDataUtils.getappointment(cat);
    Resource_Type__c resTypass = TestDataUtils.getResourceTypeAssistant();
        Contact Assistant=TestDatautils.getAssistant(resTypass);
        Resource_Type_Appointment_Type_Mapping__c restypaptyp1=TestDatautils.getresaptypemapping(resTypass,atype);
         Location_Category_Mapping__c Locat1=TestDataUtils.getlocationmapping(loc,Cat,Assistant);
            Resource_Type__c restypehuman=TestDataUtils.getResourceType();
            Resource_Type__c restype=TestDataUtils.getResourceTypeNH();
         Resources__c resour=TestDataUtils.getresources(loc, restype);
         Resource_Type_Appointment_Type_Mapping__c ResApmap=TestDataUtils.getresaptypemapping(restype, atype);    
        TestDataUtils.insertWorkingTimeSlot(sp, restypehuman, Locat);
        TestDataUtils.geteventappointmentwithall(Sp,Locat,atype);
    Datetime Datetimenow=datetime.now();
        //do POST request
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse();
        
        req.requestURI = '/services/apexrest/endUserWidget/save';
        req.httpMethod = 'POST';
    req.addParameter('alsoAddMeToWaitlist','false');
        req.addParameter('appointmentDate',String.valueof(Datetimenow.day())+','+String.valueof(Datetimenow.month())+','+String.valueof(Datetimenow.year()));
        req.addParameter('isAnyPractitioner','false');
        req.addParameter('isClassApp','false');
        req.addParameter('newappBirthDate','');
        req.addParameter('newappPhone','654132165845');
        req.addParameter('newappemail','Caop@gmail.com');
        req.addParameter('newappfname','Coap');
        req.addParameter('newapplname','Test');
        req.addParameter('startTime','13:00');
        req.addParameter('endTime','14:00');
        req.addParameter('newappPhone','555555555555');
        req.addParameter('newappComments','sample desc');
        req.addParameter('selectLocationfornewapp',loc.id);
        req.addParameter('selectCategoryfornewapp',cat.id);
        req.addParameter('selectatfornewapp',atype.id);
        req.addParameter('selectPractitionerfornewapp',sp.id);
        RestContext.request = req;
        RestContext.response = res;

        endUserWidgetSaveAPI.saveAppointment();
    System.assertNotEquals(null, res.responseBody);
    }
}