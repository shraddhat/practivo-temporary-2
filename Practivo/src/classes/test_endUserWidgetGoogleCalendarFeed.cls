@isTest
public class test_endUserWidgetGoogleCalendarFeed {

    public static string requestURI = '/services/apexrest/endUserWidget/googleCalendarFeed';
    static testMethod void 	testvalidateContact()
    {
        //do POST request
        Resource_Type__c resTyp = TestDataUtils.getResourceType();
        Contact servicePrvider  = TestDataUtils.getServiceProviderWithResTyp(resTyp);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse(); 
        req.requestURI = requestURI;
        req.httpMethod = 'POST';
        req.addParameter('method','validContact');
        req.addParameter('serviceProviderId',servicePrvider.id);
        RestContext.request = req;
        RestContext.response = res;
        endUserWidgetGoogleCalendarFeed.validateContact();
   		System.assertNotEquals(null, res.responseBody);	
    }
     static testMethod void testvalidateContactUpdate()
    {
        //do POST request
        Resource_Type__c resTyp = TestDataUtils.getResourceType();
        Contact servicePrvider  = TestDataUtils.getServiceProviderWithResTyp(resTyp);
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse(); 
        req.requestURI = requestURI;
        req.httpMethod = 'POST';
        req.addParameter('method','updateContact');
        req.addParameter('serviceProviderId',servicePrvider.id);
        RestContext.request = req;
        RestContext.response = res;
        endUserWidgetGoogleCalendarFeed.validateContact();
   		System.assertNotEquals(null, res.responseBody);	
    }
     static testMethod void testGetAppointmentsForGoogleFeed()
    {
        Location__c testLocation  = TestDataUtils.getlocation();
        Configuration_Labels__c customerNameCS = new Configuration_Labels__c(Name='Customer',value__c='Patient');
        insert customerNameCS;
        Category__c testCategory = TestDataUtils.getcategory();
        Resource_Type__c resTyp = TestDataUtils.getResourceType();
        Contact servicePrvider  = TestDataUtils.getServiceProviderWithResTyp(resTyp);
        Location_Category_Mapping__c lcm = TestDataUtils.getlocationmapping(testLocation,testCategory,servicePrvider);
        AppointmentType__c appTyp =  TestDataUtils.getappointment(testCategory);
        Resource_Type_Appointment_Type_Mapping__c resTypMap =TestDataUtils.getresaptypemappingSpecific(resTyp,appTyp,servicePrvider);
        Resource_Type__c resTypNH = TestDataUtils.getResourceTypeNH();
        Resource_Type__c resTypass = TestDataUtils.getResourceTypeAssistant();
        Contact assistant=TestDataUtils.getAssistant(resTypass);
        Location_Category_Mapping__c Locat1=TestDataUtils.getlocationmapping(testLocation,testCategory,Assistant);
        TestDataUtils.getresaptypemapping(resTypNH,appTyp);
        TestDatautils.getresaptypemapping(resTypass,appTyp);
        Event e1=TestDataUtils.insertWorkingTimeSlot(servicePrvider, resTyp, lcm);
        TestDataUtils.insertWorkingTimeSlot(assistant, resTypass, Locat1);
        Resources__c r2 = TestDataUtils.getresources(testLocation,resTypNH);
        Resources__c r3 = TestDataUtils.getresources(testLocation,resTypNH);
        Resources__c r4 = TestDataUtils.getresources(testLocation,resTypNH);
        Resources__c r5 = TestDataUtils.getresources(testLocation,resTypNH);
        Booked_Resources__c br = TestDataUtils.getbookedresources();
        TestDataUtils.getactualbooked(r2,br);
        TestDataUtils.getactualbookedAssistant(assistant, br);
        TestDataUtils.getAppointmentWithBookedResource(servicePrvider,lcm,appTyp,br);
        Datetime Datetimenow=datetime.now();
        RestRequest req = new RestRequest(); 
        RestResponse res = new RestResponse(); 
        req.requestURI = requestURI;
        req.httpMethod = 'GET';
        RestContext.request = req;
        RestContext.response = res;
        endUserWidgetGoogleCalendarFeed.getAppointmentsForGoogleFeed();
   		System.assertNotEquals(null, res.responseBody);	
    }
}