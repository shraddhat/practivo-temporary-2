@RestResource(urlMapping='/Appointed/endUserWidget/branding')  ///Appointed
global class endUserWidgetBrandingAPI{
     
     /* 
        Author : Darshan Chhajed.
        Discription : This API is for returning Branding information for End User Widget. 
                      If in parameter 'brandingCallFor' if it finds 'getBookAppointmentButtonInfo' Parameter then it returns style and text of 'Book Appointment Button' Brand.
                      If in parameter 'brandingCallFor' if it finds 'getBrandingEndUserWidget' then it returns entire branding info.          
     */
     @HttpPost
     global static void getBrandingInformation()
     {
        string getBookAppointmentButtonInfo = 'getBookAppointmentButtonInfo';
        string getBrandingEndUserWidget  = 'getBrandingEndUserWidget';
        string returnValue;
        map<String,Object> returnMap = new map<String,Object>();
        try{
                RestRequest httpRequest = RestContext.request;
                RestResponse httpResponse = RestContext.response;
                map<string,string> brandingDataRequestMap =  new map<String,string>(); 
                for(string str :  RestContext.request.params.KeySet())
                {
                    brandingDataRequestMap.put(str, RestContext.request.params.get(str));
                }
                system.debug('** endUserWidgetBrandingAPI : getBrandingInformation : Request Data **'+ brandingDataRequestMap);
                if(brandingDataRequestMap.containsKey('brandingCallFor') && brandingDataRequestMap.get('brandingCallFor') == getBookAppointmentButtonInfo)
                {
                    map<string,Branding__c> brandingSettingMap = Branding__c.getAll();
                    if(brandingSettingMap!=null)
                    {  
                        if(brandingSettingMap.containsKey('Book_Appointment_Button_Color'))
                            returnMap.put('Book_Appointment_Button_Color',brandingSettingMap.get('Book_Appointment_Button_Color').value__c);
                        if(brandingSettingMap.containsKey('Book_Appointment_Button_Text'))
                            returnMap.put('Book_Appointment_Button_Text',brandingSettingMap.get('Book_Appointment_Button_Text').value__c);
                        if(brandingSettingMap.containsKey('Book_Appointment_Button_Text_Color'))
                            returnMap.put('Book_Appointment_Button_Text_Color',brandingSettingMap.get('Book_Appointment_Button_Text_Color').value__c);      
                    }
                }
                else if(brandingDataRequestMap.containsKey('brandingCallFor') && brandingDataRequestMap.get('brandingCallFor') == getBrandingEndUserWidget)
                {
                      returnMap = Utils.getBrandingData();
                }
                returnValue = JSON.serialize(returnMap);
                system.debug('**endUserWidgetBrandingAPI : returnValue**'+returnValue);
                RestContext.response.addHeader('Content-Type', 'application/json');
                RestContext.response.addHeader('Access-Control-Allow-Origin', '*');
                RestContext.response.addHeader('Access-Control-Allow-Methods', '*');
                RestContext.response.responseBody = Blob.valueOf(returnValue);
        }catch(Exception e)
        {
            system.debug('Error : ' + e.getMessage() + ' Trace-->' + e.getStackTraceString());
        }
        
     }
}