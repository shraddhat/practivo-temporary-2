@istest
public class Test_savehelper 
{
 static testmethod void testSavehelper()
    {
        Contact con =  new Contact();
            con.FirstName='Anil';
            con.LastName = 'Dutt45gdfyg54';
            con.Email = 'anil@swiftsestufghgp.com';
           // con.RecordType.Developername='Customer';
            insert con;
            Contact con1 =  new Contact();
            con1.FirstName = 'Axnil';
            con1.LastName = 'Dxutt';
            con1.Email = 'anil@xx.com';
            insert con1;
            Contact con2 =  new Contact();
            con2.FirstName = 'Axnil';
            con2.LastName = 'Dxutt5yedfgbr5y5';
            con2.Email = 'anil@fgjhre53ewhtfxx.com';
            insert con2;
           set <id> setconid= new set<id>();

                setconid.add(con2.id);
                setconid.add(con1.id);

            Event Eventdetails=new event();
            Eventdetails.subject='sample';
            Eventdetails.Event_Type__c='Appointment';
            Eventdetails.whoid=con.Id;
            Eventdetails.IsRecurrence=false;
            Eventdetails.StartDateTime=Datetime.valueof('2016-01-30 01:00:00');
            Eventdetails.EndDateTime=Datetime.valueof('2016-01-30 02:00:00');
            insert Eventdetails;
      
        test.startTest();
        boolean result=SaveHelper.insertEventRelations(Eventdetails,setconid);
        system.assertEquals(true, result);
        test.stopTest();
    }
}