/*
***************************************
 class name-GenerateCalendarAttachment
 Class Description-Class for creating the attachment of calendar for particular Service Provider.
 *************************************
*/
public class GenerateCalendarAttachment 
{

    public static list<Event> listappointments{get;set;}
    public static list<Eventrelation> listeventrelations{get;set;}
    public Set<ID> SetattachmentIDs=new Set<ID>();
    public static List<contact> listcon{get;set;}
    public GenerateCalendarAttachment(Set<Id> Setcontactid) 
    {   
        //This method accepts set of service provider ids as parameter to generate attachment for them.
        Set<ID> Setwhowhatids=new Set<ID>();
        try
        {
              list<Attachment> listcurrentattachment;
               if(Utils.getReadAccessCheck('Attachment',new String []{'Id','name','body','description'}))
                    listcurrentattachment = [SELECT ID,name,body,ContentType,description,ParentID  FROM Attachment WHERE Description=:String.escapeSingleQuotes('Ical feed') and ParentID in :Setcontactid LIMIT 2000];  
               else
                    throw new Utils.ParsingException('No Read access to Attachment or Fields in Attachment.');
                if(Utils.getReadAccessCheck('Event',new String []{'Id','IsRecurrence','whatid', 'StartDateTime','EndDateTime','WhoId', 'Event_Type__c','Location_Category_Mapping__c'}))    
                    listappointments=[SELECT ID,whoid,whatid,Location_Category_Mapping__r.Location__r.Name,Appointment_Type__r.name,Event_Type__c,
                                         StartDateTime,EndDateTime,Appointment_Type__r.Maximum_Participant__c FROM Event  
                                         WHERE  whoid IN :Setcontactid and (Event_Type__c=:String.escapeSingleQuotes(Utils.EventType_Appointment) OR Event_Type__c=:String.escapeSingleQuotes(Utils.EventType_Partial) OR Event_Type__c=:String.escapeSingleQuotes(Utils.EventType_Unavailable)) and IsRecurrence=false LIMIT 2000];
               else
                   throw new Utils.ParsingException('No Read access to Event or Fields in Event.'); 
               if(listappointments!=null && !listappointments.isEmpty()) 
               for(Event ev:listappointments)
               {
                    if(ev.whoid!=null)
                    {
                        Setwhowhatids.add(ev.whoid);
                    }
                    if(ev.whatid!=null)
                    {
                        Setwhowhatids.add(ev.whatid);
                    }
               }
               if(Utils.getReadAccessCheck('eventrelation',new String []{'Id','relationid','eventid'}))
                    listeventrelations=[SELECT id,relationid,eventid FROM eventrelation WHERE relationid NOT IN :Setwhowhatids LIMIT 2000];
               else
                    throw new Utils.ParsingException('No Read access to eventrelation or Fields in eventrelation.');
               Set<Id> SetattachmentIDs = (new Map<Id,SObject>(listcurrentattachment)).keySet();  
               if(Utils.getReadAccessCheck('contact',new String []{'Id','name','firstname','RecordTypeId'})) 
                    listcon=[SELECT id,name,firstname,RecordTypeId,RecordType.Developername FROM contact WHERE (Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_CustomerRecordType) OR Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_ServiceProviderRecordType) OR Recordtype.developername=:String.escapeSingleQuotes(Utils.contact_AssistantRecordType)) LIMIT 2000];  
               else
                    throw new Utils.ParsingException('No Read access to contact or Fields in contact.');                          
            list<Attachment> att=new list<Attachment>();
            map<Id,Attachment> MapAttachmentWithParent = new map<Id,Attachment>();
                if(listcurrentattachment.isEmpty())
                {
                    for(ID contactid:Setcontactid)
                    {
                        att.add(new Attachment
                        (
                            Body=Blob.valueof(doIcsAttachment(contactid)),
                            ContentType=String.valueof('text/calendar'),
                            Name=String.valueof('meeting.ics'),
                            description='Ical feed',
                            parentId=contactid
                        )
                        );
                    }
                    if(Utils.getCreateAccessCheck('Attachment', new String []{'Body','ContentType','Name','description','parentId'}))
                        insert att;
                    else
                        throw new Utils.ParsingException('No Create access to Attachment or Fields in Attachment.');    
                }
                else
                {
                        for(Attachment a:listcurrentattachment)
                        {
                            MapAttachmentWithParent.put(a.parentId,a);
                        }
                        for(ID contactid:Setcontactid)
                        {
                            if(MapAttachmentWithParent.containsKey(contactid))
                            {
                                Attachment a=MapAttachmentWithParent.get(contactid);
                                a.body=Blob.valueof(doIcsAttachment(a.parentId));
                                a.description='Ical feed';
                                att.add(a);
                            }
                            else
                            {
                                att.add(new Attachment
                                (
                                    Body=Blob.valueof(doIcsAttachment(contactid)),
                                    ContentType=String.valueof('text/calendar'),
                                    Name=String.valueof('meeting.ics'),
                                    description='Ical feed',
                                    parentId=contactid
                                )
                                );
                            }
                        }
                    if(Utils.getCreateAccessCheck('Attachment', new String []{'Body','ContentType','Name','description','parentId'}) && Utils.getUpdateAccessCheck('Attachment', new String []{'Body','ContentType','Name','description','parentId'}))    
                    upsert att;
                    else
                        throw new Utils.ParsingException('No Create access to Attachment or Fields in Attachment.');
                }
        }
        catch(exception ex)
        {
            System.debug('GenerateCalendarAttachment-->'+ex.getmessage()+ex.getstacktracestring());
        }
    }
    
    /*
     @MethodName : doIcsAttachment
     @Return Type :  String (ics file)  
     @Author : Koustubh Kulkarni.
     @Description : This method is used to generate ics file body for particular service provider which is stored in attachment object against that SP.       
   */
    public static string doIcsAttachment(ID contactID) 
    {
    String txtInvite;
    String Customername;
    String FormattedStartDate;
    String FormattedEndDate;
    String patientname;
    string attachment;
    integer uidno;
    String UserTimezone;
    TimeZone tz = UserInfo.getTimeZone();
    UserTimezone=tz.getID();
    try
    {
        list<Event> listappointmentsforServiceProvider=new list<Event>();
        Configuration_Labels__c LabelChangerSettingsCustomer= Configuration_Labels__c.getInstance('Customer');
            for(Event ev:listappointments)
            {
                if(ev.whoid==contactID)
                {
                  listappointmentsforServiceProvider.add(ev);  
                }
            }
            txtInvite='BEGIN:VCALENDAR\n'+'PRODID:-//Icalendar//ePractice\n'+'VERSION:2.0\n'+'X-WR-TIMEZONE:'+UserTimezone+'\n'+'CALSCALE:GREGORIAN\n'+'METHOD:PUBLISH\n';
            if(!listappointmentsforServiceProvider.isEmpty())
            {
            for(Event app: listappointmentsforServiceProvider)
                   {
                        FormattedStartDate=getformateddate(app.StartDateTime );
                        FormattedEndDate=getformateddate(app.EndDateTime);
                        txtInvite += 'BEGIN:VEVENT\n';
                        txtInvite += 'DTSTART;TZID='+UserTimezone+':'+FormattedStartDate+'\n';
                        txtInvite += 'DTEND;TZID='+UserTimezone+':'+FormattedEndDate+'\n';
                        if(app.Appointment_Type__r.Maximum_Participant__c>1 && app.Event_Type__c!=Utils.EventType_Unavailable)
                        {
                            txtInvite += 'DESCRIPTION:Hi '+getname(app.whoid)+',\\nHere are the '+String.valueOf(LabelChangerSettingsCustomer.value__c)+' names for your group appointment:\\n ';
                        }
                        else if(app.Appointment_Type__r.Maximum_Participant__c <=1 && app.Event_Type__c!=Utils.EventType_Unavailable)
                        {
                            txtInvite += 'DESCRIPTION:Hi '+getname(app.whoid)+',\\nYou have new appointment. Appointment details are:-\\n '+String.valueOf(LabelChangerSettingsCustomer.value__c)+' name-\\n ';
                        }
                        else if(app.Event_Type__c==Utils.EventType_Unavailable)
                        {
                            txtInvite += 'DESCRIPTION:Hi '+getname(app.whoid)+',\\nThis is your unavailable timeslot.\\n ';
                        }
                         if(!listeventrelations.isEmpty() && app.Event_Type__c!=Utils.EventType_Unavailable)
                        {
                            for(eventrelation er:listeventrelations)
                            {
                                if(app.ID==er.Eventid)
                                {
                                    Customername=getname(er.relationid);
                                    txtInvite +=Customername+'\\n';
                                }
                            }
                        }
                        txtInvite +='-Thanks.\n';
                        //txtInvite += 'Appointment Type:- '+app.Appointment_Type__r.name+'\n Thanks\n';
                        txtInvite += 'LOCATION:'+app.Location_Category_Mapping__r.Location__r.Name+'\n';
                        txtInvite += 'PRIORITY:5\n';
                        txtInvite += 'SEQUENCE:0\n';
                        if(app.Appointment_Type__r.Maximum_Participant__c>1 && app.Event_Type__c!=Utils.EventType_Unavailable)
                        {
                            txtInvite += 'SUMMARY:'+app.Appointment_Type__r.name+'\n';
                        }
                        else if(app.Appointment_Type__r.Maximum_Participant__c <=1 && app.Event_Type__c!=Utils.EventType_Unavailable)
                        {
                            txtInvite += 'SUMMARY:'+app.Appointment_Type__r.name+' with '+Customername+'\n';
                        }
                        else if(app.Event_Type__c==Utils.EventType_Unavailable)
                        {
                            txtInvite += 'SUMMARY: Unavailable timeslot'+'\n';
                        }
                        txtInvite += 'TRANSP:OPAQUE\n';
                        txtInvite += 'UID:'+'2016'+String.valueOf(app.ID)+'@ePractice\n';
                        txtInvite += 'STATUS:CONFIRMED\n';
                        txtInvite += 'END:VEVENT\n'; 
                   }
             }      
               attachment =txtInvite+string.valueof('END:VCALENDAR');
               system.debug('attachment'+attachment);
    }
    catch(exception ex)
    {
        system.debug('GenerateCalendarAttachment-->doIcsAttachment-->'+ex.getmessage()+ex.getstacktracestring());
    }
        return attachment; 
    }
    private static string getname(id whoid)
        {
            string name;
            try
            {
                for(contact con:listcon)
                {
                    if(con.id==whoid && con.RecordType.Developername==Utils.contact_CustomerRecordType)
                    {
                        name=con.Name;
                    }
                    else if(con.id==whoid && con.RecordType.Developername==Utils.contact_ServiceProviderRecordType)
                    {
                        name=con.Firstname;
                    }
                }
            }
            catch(exception ex)
            {
                system.debug('GenerateCalendarAttachment-->getname-->'+ex.getmessage());
            }
            return name;
        }
     public static string getformateddate(Datetime appdate)
         {
            //This method converts given date in ical supported format which is like follow
            //2015-12-01T00:00:00
            //2015-12-10T00:00:00
             String s;
             String s1;
             String s2;
             String s3;
             String s4;
             String s5;
             String s6;
             String s7;
             String formattedstring;
            
             Date myDate = date.newinstance(appdate.year(), appdate.month(), appdate.day());
             s=string.valueof(myDate);
             s1=s.mid(0,4);
             s2=s.mid(5,2);
             s3=s.mid(8,2);
             s4=string.valueof(appdate);
             s5=s4.mid(11,2);
             s6=s4.mid(14,2);
             s7=s4.mid(17,2);
             formattedstring=s1+s2+s3+'T'+s5+s6+s7;
        
        return formattedstring;
         }
}