/*
This is post install script which is used to insert all the default values for Laberchanger custom settings. 
*/
global class PostInstallClass implements InstallHandler {
  global void onInstall(InstallContext context) {
   if(context.previousVersion() == null) {
          PostInstallClasshandler.insertCustomSettingvalues();
      }
    else
      if(context.previousVersion().compareTo(new Version(1,0)) == 0) {
      PostInstallClasshandler.insertCustomSettingvalues();
      }
    if(context.isUpgrade()) {
      PostInstallClasshandler.insertCustomSettingvalues();
      }
    if(context.isPush()) {
      PostInstallClasshandler.insertCustomSettingvalues();
      }
    }
    
  }